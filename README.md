# Library CRUD

This project is an example of a CRUD (Create, Read, Update, Delete) application for a library, developed using Visual Studio, SQL Server, C#, JavaScript, and ASP.NET Core MVC.

## Technologies Used

- **Visual Studio**: IDE used for the project development.
- **SQL Server**: Relational database management system used for storing the application data.
- **C#**: Programming language used for the backend development of the application.
- **JavaScript**: Programming language used to add interactivity to the client-side of the application.
- **Fetch API**: Used to make asynchronous HTTP requests from JavaScript.
- **DOM (Document Object Model)**: Used to access and manipulate HTML elements of the page from JavaScript.
- **HTML and CSS**: Used for the structure and styling of the application.
- **Bootstrap**: CSS framework used for responsive design of the user interface.
- **ASP.NET Core MVC**: Used as the architectural pattern to organize the application into models, views, and controllers.
- **SQLClient**: Used for database connectivity and operations within the C# code.

## Features

The application includes the following features:

- **Create**: Allows adding new books to the library.
- **Read**: Displays the list of books available in the library.
- **Update**: Allows modifying the information of an existing book.
- **Delete**: Allows deleting books from the library.

## Project Structure

The project is organized as follows:

- **Models**: Contains C# classes representing the domain objects of the application, such as the book model.
- **Views**: Contains HTML views used to display the user interface to the end user. Partial views are used to reuse HTML code.
- **Controllers**: Contains C# controllers that handle HTTP requests and control the flow of the application.
- **Scripts**: Contains database scripts used for setup and initialization, located in the 'Scripts' folder within the project.
- **Configuration**: Contains application configuration files, such as the connection string to the database.

## Usage Instructions

1. Clone the repository to your local machine.
2. Open the project in Visual Studio.
3. Configure the connection string to your SQL Server database in the `appsettings.json` file.
4. Execute the database creation script on your SQL Server instance to create the database structure.
5. Run the application from Visual Studio.
6. Navigate through the different views to create, read, update, and delete books in the library.

## Contribution

Contributions are welcome! If you'd like to improve the application, you can fork the repository, make your changes, and submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
