﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAppJsMVC.Models.DAO;
using WebAppJsMVC.Models.DataAccess;

namespace WebAppJsMVC.Controllers
{
    public class BookController : Controller
    {

        DABook dataAccess;
        readonly string _connectionString;
        public BookController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess= new DABook(_connectionString); 
        }

        public ActionResult Items()
        {
            return View();
        }

        public ActionResult getListItems(int page, int perPage)
        {
            int totalRows = dataAccess.CountItems();
            // Cuando el pagineo sea mayor a 1, se debe hacer el calculo de los registros a excluir.
            int lastRowOfPreviousPage = page > 1 ? (page - 1) * perPage : 0;

            object result = new
            {
                total = totalRows,
                rows = dataAccess.ReadByPage(lastRowOfPreviousPage, perPage)
            };
            return Json(result);
        }

        public ActionResult getSimpleItems()
        {
            try
            {
                List<Book> list = dataAccess.ReadBasicData();
                return Json(new { id = 0, items = list });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        public ActionResult getItemsById(int id)
        {
            Book book = dataAccess.ReadSingle(id);
            return Json(book);
        }

        // GET: BookController
        public ActionResult Index()
        {
            List<Book> lista = dataAccess.Read();
            return View(lista);
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            Book book = dataAccess.ReadSingle(id);
            return View(book);
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] Book book)
        {
            try
            {
                dataAccess.Create(book);    
                return Json(new { id=0, msg="Guardado correctamente!" });
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] Book book)
        {
            try
            {
                dataAccess.Update(book);
                return Json(new { id = 0, msg = "Editado correctamente!"});
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;
            try
            {
                dataAccess.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
