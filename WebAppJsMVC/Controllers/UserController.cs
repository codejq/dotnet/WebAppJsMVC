﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppJsMVC.Models.DataAccess;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Controllers
{
    public class UserController : Controller
    {
        private readonly string _connectionString;
        DAUser dataAccess;

        public UserController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DAUser(_connectionString);

        }
        // GET: UserController
        public ActionResult Items()
        {
            return View();
        }

        public ActionResult getItemsById(int id)
        {
            User user = dataAccess.ReadSingle(id);
            return Json(user);
        }

        public ActionResult getSimpleItems()
        {
            try
            {
                List<User> list = dataAccess.ReadBasicData();
                return Json(new { id = 0, items = list });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        public ActionResult getListItems(int page, int perPage)
        {
            int totalRows = dataAccess.CountItems();
            // Cuando el pagineo sea mayor a 1, se debe hacer el calculo de los registros a excluir.
            int lastRowOfPreviousPage = page > 1 ? (page - 1) * perPage : 0;

            object result = new
            {
                total = totalRows,
                rows = dataAccess.ReadByPage(lastRowOfPreviousPage, perPage)
            };
            return Json(result);
        }

        // GET: UserController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] User usuario)
        {
            try
            {
                dataAccess.Create(usuario);
                return Json(new { id=0, msg="Creado correctamente!" } );
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: UserController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] User usuario)
        {
            try
            {
                dataAccess.Update(usuario);
                return Json(new { id = 0, msg = "Creado correctamente!" });
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: UserController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GenreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;

            try
            {
                dataAccess.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
