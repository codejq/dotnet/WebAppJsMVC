﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppJsMVC.Models.DataAccess;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Controllers
{
    public class CountryController : Controller
    {
        DACountry dataAccess;
        private readonly string _connectionString;
        public CountryController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DACountry(_connectionString);
        }


        // GET: CountryController
        public ActionResult getItems()
        {
            List<Country> list = dataAccess.Read();
            return Json(list);
        }

        public ActionResult getSimpleItems()
        {
            try
            { 
                return Json(new { id = 0, items = dataAccess.ReadBasicData() });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }


        public ActionResult Items()
        {
            return View();
        }

        // GET: CountryController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CountryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CountryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] Country country)
        {
            object result;
            try
            {
                dataAccess.Create(country);
                result = new { id = 0, msg = "Creado correctamente!" };
                return Json(result);
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
                return Json(result);
            }
        }

        // GET: CountryController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        public ActionResult getItemsById(int id)
        {
            Country country = dataAccess.ReadSingle(id);
            return Json(country);
        }

        [HttpPost]
        public ActionResult getSingleCountry([FromBody] int id)
        {
            Country country = dataAccess.ReadSingle(id);
            return Json(country);
        }

        // POST: CountryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] Country country)
        {
            object result;
            try
            {
                dataAccess.Update(country);
                result = new { id = 0, msg = "Operación finalizada correctamente." };
            }
            catch (Exception e)
            {
                result = new { id = 1, msg = e.Message };
            }
            return Json(result);
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CountryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;
            try
            {
                dataAccess.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
