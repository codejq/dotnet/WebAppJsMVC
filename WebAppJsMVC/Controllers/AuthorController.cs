﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using WebAppJsMVC.Models.DAO;
using WebAppJsMVC.Models.DataAccess;

namespace WebAppJsMVC.Controllers
{
    public class AuthorController : Controller
    {

        private readonly string _connectionString;
        DAAuthor dataAccess;
        public AuthorController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DAAuthor(_connectionString);
        }

        public ActionResult Items()
        {
            return View();
        }

        public ActionResult getListItems(int page, int perPage)
        {
            int totalRows = dataAccess.CountItems();
            // Cuando el pagineo sea mayor a 1, se debe hacer el calculo de los registros a excluir.
            int lastRowOfPreviousPage = page > 1 ? (page - 1) * perPage : 0;

            object result = new
            {
                total = totalRows,
                rows = dataAccess.ReadByPage(lastRowOfPreviousPage, perPage)
            };
            return Json(result);
        }

        // GET: AuthorController
        public ActionResult getSimpleItems()
        {
            try
            {
                List<Author> list = dataAccess.ReadBasicData();
                return Json(new { id = 0, items = list });
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        public ActionResult getItemsById(int id)
        {
            Author author = dataAccess.ReadSingle(id);
            return Json(author);
        }

        // GET: AuthorController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AuthorController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AuthorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] Author author)
        {
            try
            {
                dataAccess.Create(author);
                return Json(new { id = 0, msg = "Almacenado correctamente!" });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: AuthorController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AuthorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] Author author)
        {
            try
            {
                dataAccess.Update(author);
                return Json(new {id=0, msg="Editado correctamente!" });
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: AuthorController/Delete/5
        public ActionResult Delete(int id)
        { 
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;
            try
            {
                dataAccess.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
