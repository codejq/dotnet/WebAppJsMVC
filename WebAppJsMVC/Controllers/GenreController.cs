﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Globalization;
using WebAppJsMVC.Models.DataAccess;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Controllers
{
    public class GenreController : Controller
    {

        DAGenre daGenre;

        public GenreController(IConfiguration configuration)
        {
            string connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            daGenre = new DAGenre(connectionString);
        }

        public ActionResult Items()
        {
            return View();
        }

        // GET: GenreController
        public ActionResult getItems()
        {
            List<Genre> genres = daGenre.Read();
            return Json(genres);
        }

        public ActionResult getSimpleItems()
        {
            try
            {
                List<Genre> list = daGenre.ReadSimpleData();
                return Json(new { id = 0, items = list });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: GenreController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult getItemsById(int id)
        {
            Genre genre = daGenre.ReadSingle(id);
            return Json(genre);
        }


        // GET: GenreController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GenreController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] Genre genre)
        {
            object result;

            try
            {
                daGenre.Create(genre);
                result = new { id = 0, msg = "Creado correctamente!" };
                return Json(result);
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
                return Json(result);
            }
        }

        // GET: GenreController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: GenreController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] Genre genre)
        {
            object result;
            try
            {
                daGenre.Update(genre);
                result = new { id = 0, msg = "Actualizado correctamente!" };
            }
            catch(Exception ex) 
            {
                result = new { id = -1, msg = ex.Message };
            }
            return Json(result);
        }

        // GET: GenreController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GenreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;

            try
            {
                daGenre.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch(Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
