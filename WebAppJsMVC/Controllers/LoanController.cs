﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAppJsMVC.Models.DAO;
using WebAppJsMVC.Models.DataAccess;

namespace WebAppJsMVC.Controllers
{
    public class LoanController : Controller
    {
        DALoan dataAccessLoan;
        private readonly string _connectionString;
        public LoanController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccessLoan = new DALoan(_connectionString);
        }

        public ActionResult Items()
        {
            return View();
        }

        public ActionResult getListItems(int page, int perPage)
        {
            int totalRows = dataAccessLoan.CountItems();
            // Cuando el pagineo sea mayor a 1, se debe hacer el calculo de los registros a excluir.
            int lastRowOfPreviousPage = page > 1 ? (page - 1) * perPage : 0;

            object result = new
            {
                total = totalRows,
                rows = dataAccessLoan.ReadByPage(lastRowOfPreviousPage, perPage)
            };
            return Json(result);
        }


        public ActionResult getItemsById(int id)
        {
            Loan loan = dataAccessLoan.ReadSingle(id);
            return Json(loan);
        }


        // GET: LoanController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoanController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LoanController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromBody] Loan loan)
        {
            try
            {
                dataAccessLoan.Create(loan);
                return Json(new { id = 0, msg = "Guardado Correctamente!" });
            }
            catch (Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: LoanController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoanController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromBody] Loan loan)
        {
            try
            {
                dataAccessLoan.Update(loan);
                return Json(new { id = 0, msg = "Editado correctamente!" });
            }
            catch(Exception ex)
            {
                return Json(new { id = -1, msg = ex.Message });
            }
        }

        // GET: LoanController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteById([FromBody] int id)
        {
            object result;
            try
            {
                dataAccessLoan.Delete(id);
                result = new { id = 0, msg = "" };
            }
            catch (Exception ex)
            {
                result = new { id = -1, msg = ex.Message };
            }

            return Json(result);
        }
    }
}
