﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Models.DataAccess
{
    public class DAGenre
    {
        private readonly string connectionString;

        public DAGenre(string connectionString)
        {
            this.connectionString = connectionString;
        }


        public bool Create(Genre genre)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_GENRES(name) VALUES (@name)";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", genre.Nombre);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        public List<Genre> Read()
        {
            try
            {
                List<Genre> genres = new List<Genre>();
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_genre, name FROM TBL_GENRES";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre genre = new Genre();
                                genre.Id = reader.GetInt32("id_genre");
                                genre.Nombre = reader.GetString("name");
                                genres.Add(genre);
                            }
                        }
                    }
                    return genres;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mensaje de Error: " + ex.Message);
                throw;
            }
        }


        public List<Genre> ReadSimpleData()
        {
            try
            {
                List<Genre> genres = new List<Genre>();
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_genre, name FROM TBL_GENRES";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre genre = new Genre();
                                genre.Id = reader.GetInt32("id_genre");
                                genre.Nombre = reader.GetString("name");
                                genres.Add(genre);
                            }
                        }
                    }
                    return genres;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mensaje de Error: " + ex.Message);
                throw;
            }
        }


        public Genre ReadSingle(int id)
        {
            try
            {
                Genre genre = new Genre();
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_genre, name FROM TBL_GENRES WHERE id_genre = @id_genre";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_genre", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                genre.Id = reader.GetInt32("id_genre");
                                genre.Nombre = reader.GetString("name");
                            }
                        }
                    }
                    return genre;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mensaje de Error: " + ex.Message);
                throw;
            }
        }


        public bool Update(Genre genre)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_GENRES SET name=@name WHERE id_genre=@id_genre";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_genre", genre.Id);
                        command.Parameters.AddWithValue("@name", genre.Nombre);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_GENRES WHERE id_genre=@id_genre";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_genre", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
