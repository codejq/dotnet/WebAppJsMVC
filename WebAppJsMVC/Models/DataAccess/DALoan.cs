﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Models.DataAccess
{
    public class DALoan
    {
        private readonly string _connectionString;

        public DALoan(string connectionString)
        {
            _connectionString = connectionString;
        }


        public int CountItems()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT COUNT(1) FROM TBL_LOANS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            return (int)result;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Loan> ReadByPage(int lastRowOfPreviousPage, int perPage)
        {
            try
            {
                List<Loan> list = new List<Loan>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = $@"SELECT l.id_loan, l.loan_date, l.return_date, u.id_user, u.firstname, u.lastname, b.id_book, b.title
                                    FROM TBL_LOANS l
                                    JOIN TBL_USERS u ON u.id_user = l.id_user
                                    JOIN TBL_BOOKS b ON b.id_book = l.id_book
                                    ORDER BY l.id_loan
                                    OFFSET {lastRowOfPreviousPage} ROWS
                                    FETCH NEXT {perPage} ROWS ONLY
                                    ";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Loan entity = new Loan();
                                entity.Id = reader.GetInt32("id_loan");
                                entity.LoanDate = DateOnly.FromDateTime(reader.GetDateTime("loan_date"));
                                entity.ReturnDate = DateOnly.FromDateTime(reader.GetDateTime("return_date"));
                                entity.User = new User() { Id = reader.GetInt32("id_loan"), FirstName = reader.GetString("firstname"), LastName = reader.GetString("lastname") };
                                entity.Book = new Book() { Id = reader.GetInt32("id_book"), Title = reader.GetString("title") };
                                list.Add(entity);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public List<Loan> Read()
        {
            try
            {
                List<Loan> list = new List<Loan>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT l.id_loan, l.loan_date, l.return_date, u.id_user, u.firstname, u.lastname, b.id_book, b.title
                                    FROM TBL_LOANS l
                                    JOIN TBL_BOOKS b ON b.id_book = l.id_book
                                    JOIN TBL_USERS u ON u.id_user = l.id_user
                                    ORDER BY l.loan_date DESC";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Loan loan = new Loan();
                                loan.Id = reader.GetInt32("id_loan");
                                loan.LoanDate = DateOnly.FromDateTime(reader.GetDateTime("loan_date"));
                                loan.ReturnDate = DateOnly.FromDateTime(reader.GetDateTime("return_date"));
                                // Usuario
                                loan.User = new User()
                                {
                                    Id = reader.GetInt32("id_user"),
                                    FirstName = reader.GetString("firstname"),
                                    LastName = reader.GetString("lastname")
                                };
                                // Libro
                                loan.Book = new Book()
                                {
                                    Id = reader.GetInt32("id_book"),
                                    Title = reader.GetString("title")
                                };
                                list.Add(loan);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public Loan ReadSingle(int id)
        {
            try
            {
                Loan loan = new Loan();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT l.id_loan, l.loan_date, l.return_date, u.id_user, u.firstname, u.lastname, b.id_book, b.title
                                    FROM TBL_LOANS l
                                    JOIN TBL_BOOKS b ON b.id_book = l.id_book
                                    JOIN TBL_USERS u ON u.id_user = l.id_user
                                    WHERE id_loan = @id_loan";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_loan", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                loan.Id = reader.GetInt32("id_loan");
                                loan.LoanDate = DateOnly.FromDateTime(reader.GetDateTime("loan_date"));
                                loan.ReturnDate = DateOnly.FromDateTime(reader.GetDateTime("return_date"));
                                // Usuario
                                loan.User = new User()
                                {
                                    Id = reader.GetInt32("id_user"),
                                    FirstName = reader.GetString("firstname"),
                                    LastName = reader.GetString("lastname")
                                };
                                // Libro
                                loan.Book = new Book()
                                {
                                    Id = reader.GetInt32("id_book"),
                                    Title = reader.GetString("title")
                                };
                            }
                            return loan;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Create(Loan loan)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_LOANS(loan_date, return_date, id_user, id_book) VALUES (@loan_date, @return_date, @id_user, @id_book)";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@loan_date", loan.LoanDate);
                        command.Parameters.AddWithValue("@return_date", loan.ReturnDate);
                        command.Parameters.AddWithValue("@id_user", loan.User.Id);
                        command.Parameters.AddWithValue("@id_book", loan.Book.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Update(Loan country)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_LOANS SET loan_date=@loan_date, return_date=@return_date, id_user=@id_user, id_book=@id_book WHERE id_loan = @id_loan";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_loan", country.Id);
                        command.Parameters.AddWithValue("@loan_date", country.LoanDate);
                        command.Parameters.AddWithValue("@return_date", country.ReturnDate);
                        command.Parameters.AddWithValue("@id_user", country.User.Id);
                        command.Parameters.AddWithValue("@id_book", country.Book.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_LOANS WHERE id_loan=@id_loan";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_loan", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
