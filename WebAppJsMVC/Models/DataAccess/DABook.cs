﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Models.DataAccess
{
    public class DABook
    {
        private readonly string _connectionString;

        public DABook(string connectionString)
        {
            _connectionString = connectionString;
        }


        public int CountItems()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT COUNT(1) FROM TBL_BOOKS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            return (int)result;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Book> ReadByPage(int lastRowOfPreviousPage, int perPage)
        {
            try
            {
                List<Book> list = new List<Book>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = $@"SELECT b.id_book, b.title, b.publication_year, a.id_author, a.firstname, a.lastname, g.id_genre, g.name
                                    FROM TBL_BOOKS b
                                    JOIN TBL_AUTHORS a ON a.id_author = b.author
                                    JOIN TBL_GENRES g ON g.id_genre = b.genre
                                    ORDER BY b.id_book
                                    OFFSET {lastRowOfPreviousPage} ROWS
                                    FETCH NEXT {perPage} ROWS ONLY
                                    ";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book entity = new Book();
                                entity.Id = reader.GetInt32("id_book");
                                entity.Title = reader.GetString("title");
                                entity.PublicationYear = reader.GetInt32("publication_year");
                                entity.Author = new Author() { Id = reader.GetInt32("id_author"), FirstName = reader.GetString("firstname"), LastName = reader.GetString("lastname") };
                                entity.Genre = new Genre() { Id = reader.GetInt32("id_genre"), Nombre = reader.GetString("name") };
                                list.Add(entity);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Book> ReadBasicData()
        {
            try
            {
                List<Book> list = new List<Book>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_book, title FROM TBL_BOOKS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book book = new Book();
                                book.Id = reader.GetInt32("id_book");
                                book.Title = reader.GetString("title");
                                list.Add(book);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Book> Read()
        {
            try
            {
                List<Book> list = new List<Book>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_book, title, publication_year, author, genre FROM TBL_BOOKS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book book = new Book();
                                book.Id = reader.GetInt32("id_book");
                                book.Title = reader.GetString("title");
                                book.PublicationYear = reader.GetInt32("publication_year");
                                book.Author = new Author() { Id = reader.GetInt32("author") };
                                book.Genre = new Genre() { Id = reader.GetInt32("genre") };
                                list.Add(book);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public Book ReadSingle(int id)
        {
            try
            {
                Book book = new Book();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT b.id_book, b.title, b.publication_year, b.author, a.firstname, a.lastname, b.genre, g.name as name_genre
                                    FROM TBL_BOOKS b
                                    INNER JOIN TBL_AUTHORS a ON a.id_author = b.author
                                    INNER JOIN TBL_GENRES g ON g.id_genre = b.genre
                                    WHERE b.id_book = @id_book";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_book", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                book.Id = reader.GetInt32("id_book");
                                book.Title = reader.GetString("title");
                                book.PublicationYear = reader.GetInt32("publication_year");
                                book.Author = new Author() { Id = reader.GetInt32("author"), FirstName = reader.GetString("firstname"), LastName=reader.GetString("lastname") };
                                book.Genre = new Genre() { Id = reader.GetInt32("genre"), Nombre = reader.GetString("name_genre") };
                            }
                            return book;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Create(Book book)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_BOOKS(title, publication_year, author, genre) VALUES (@title, @publication_year, @author, @genre)";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@title", book.Title);
                        command.Parameters.AddWithValue("@publication_year", book.PublicationYear);
                        command.Parameters.AddWithValue("@author", book.Author.Id);
                        command.Parameters.AddWithValue("@genre", book.Genre.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Update(Book book)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_BOOKS SET title=@title, publication_year=@publication_year, author=@author, genre=@genre WHERE id_book = @id_book";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_book", book.Id);
                        command.Parameters.AddWithValue("@title", book.Title);
                        command.Parameters.AddWithValue("@publication_year", book.PublicationYear);
                        command.Parameters.AddWithValue("@author", book.Author.Id);
                        command.Parameters.AddWithValue("@genre", book.Genre.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_BOOKS WHERE id_book=@id_book";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_book", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
