﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppJsMVC.Models.DAO;

namespace WebAppJsMVC.Models.DataAccess
{
    public class DAAuthor
    {
        private readonly string _connectionString;

        public DAAuthor(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int CountItems()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT COUNT(1) FROM TBL_AUTHORS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            return (int)result;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Author> ReadByPage(int lastRowOfPreviousPage, int perPage)
        {
            try
            {
                List<Author> list = new List<Author>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = $@"SELECT a.id_author,a.firstname,a.lastname,a.birthday,a.country_origin, c.name as name_country_origin
                                    FROM TBL_AUTHORS a
                                    JOIN TBL_COUNTRIES c ON c.id_country = a.country_origin
                                    ORDER BY a.id_author
                                    OFFSET {lastRowOfPreviousPage} ROWS
                                    FETCH NEXT {perPage} ROWS ONLY
                                    ";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Author entity = new Author();
                                entity.Id = reader.GetInt32("id_author");
                                entity.FirstName = reader.GetString("firstname");
                                entity.LastName = reader.GetString("lastname");
                                entity.BirthDay = DateOnly.FromDateTime(reader.GetDateTime("birthday"));
                                entity.CountryOrigin = new Country() { Id = reader.GetInt32("country_origin"), Nombre = reader.GetString("name_country_origin") };
                                list.Add(entity);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Author> ReadBasicData()
        {
            try
            {
                List<Author> list = new List<Author>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT a.id_author, a.firstname, a.lastname
                                    FROM TBL_AUTHORS a ";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Author author = new Author();
                                author.Id = reader.GetInt32("id_author");
                                author.FirstName = reader.GetString("firstname");
                                author.LastName = reader.GetString("lastname");
                                list.Add(author);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public List<Author> Read()
        {
            try
            {
                List<Author> list = new List<Author>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT a.id_author, a.firstname, a.lastname, a.birthday, a.country_origin, c.name as country_name
                                    FROM TBL_AUTHORS a 
                                    JOIN TBL_COUNTRIES c ON c.id_country = a.country_origin";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Author author = new Author();
                                author.Id = reader.GetInt32("id_author");
                                author.FirstName = reader.GetString("firstname");
                                author.LastName = reader.GetString("lastname");
                                author.BirthDay = DateOnly.FromDateTime(reader.GetDateTime("birthday"));
                                author.CountryOrigin = new Country() { Id = reader.GetInt32("country_origin"), Nombre = reader.GetString("country_name") };
                                list.Add(author);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public Author ReadSingle(int id)
        {
            try
            {
                Author author = new Author();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = @"SELECT a.id_author, a.firstname, a.lastname, a.birthday, a.country_origin, c.name as country_name
                                    FROM TBL_AUTHORS a 
                                    JOIN TBL_COUNTRIES c ON c.id_country = a.country_origin
                                    WHERE a.id_author = @id_author";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_author", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                author.Id = reader.GetInt32("id_author");
                                author.FirstName = reader.GetString("firstname");
                                author.LastName = reader.GetString("lastname");
                                author.BirthDay = DateOnly.FromDateTime(reader.GetDateTime("birthday"));
                                author.CountryOrigin = new Country() { Id = reader.GetInt32("country_origin"), Nombre = reader.GetString("country_name")  };
                            }
                            return author;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Create(Author author)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_AUTHORS(firstname, lastname, birthday, country_origin) VALUES (@firstname, @lastname, @birthday, @country_origin)";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@firstname", author.FirstName);
                        command.Parameters.AddWithValue("@lastname", author.LastName);
                        command.Parameters.AddWithValue("@birthday", author.BirthDay);
                        command.Parameters.AddWithValue("@country_origin", author.CountryOrigin.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Update(Author author)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_AUTHORS SET firstname=@firstname, lastname=@lastname, birthday=@birthday, country_origin=@country_origin WHERE id_author = @id_author";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_author", author.Id);
                        command.Parameters.AddWithValue("@firstname", author.FirstName);
                        command.Parameters.AddWithValue("@lastname", author.LastName);
                        command.Parameters.AddWithValue("@birthday", author.BirthDay);
                        command.Parameters.AddWithValue("@country_origin", author.CountryOrigin.Id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_AUTHORS WHERE id_author=@id_author";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_author", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
