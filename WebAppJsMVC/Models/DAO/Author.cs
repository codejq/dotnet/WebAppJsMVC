﻿namespace WebAppJsMVC.Models.DAO
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateOnly BirthDay { get; set; }
        public Country CountryOrigin { get; set; }

        public string FullName { get
            {
                return string.Concat(FirstName, " ", LastName);
            } 
        }
    }
}
