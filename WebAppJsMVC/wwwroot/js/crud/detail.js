﻿function cargarDatos() { 
    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');
    var entidad = document.getElementById("entidad").value;


    fetch(`/${entidad}/getItemsById/` + id)
        .then(response => response.json())
        .then(data => {
            fillData(data);
        })
        .catch(error => console.error('Error', error));
}

function editar() {
    const id = document.getElementById('id').textContent;
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Edit?id=` + id;
}
 
function cancelar() { 
    window.history.back();
}
 