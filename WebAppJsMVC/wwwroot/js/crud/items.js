﻿function cargarDatos() {
    var tabla = document.getElementById("tblData");
    var tbody = tabla.getElementsByTagName("tbody")[0];
    var entidad = document.getElementById("entidad").value;

    fetch(`/${entidad}/getItems/`)
        .then(response => response.json())
        .then(data => {
            data.forEach(function (item) {
                var row = htmlRow(item);
                tbody.innerHTML += row;
            });
        })
        .catch(error => console.error('Error', error));
}

function nuevo() {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Create/`;
}

function editar(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Edit?id=` + id;
}

function verDetalle(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Details?id=` + id;
}

function eliminar(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Delete?id=` + id;
}