﻿var paginaActual = 1;
var totalPaginas = 0;
var totalRegistros = 0;
var filasxPagina = 10;

function adelante() {
    paginaActual += 1;
    document.getElementById("cmbListaPaginas").value = paginaActual;
    console.log(`Página No.: ${paginaActual}`);
    cargarDatos(false);
}

function atras() {
    if (paginaActual > 1) {
        paginaActual -= 1;
    }
    console.log(`Página No.: ${paginaActual}`);
    document.getElementById("cmbListaPaginas").value = paginaActual;
    cargarDatos(false);
}

function irUltimo() {
    paginaActual = totalPaginas;
    document.getElementById("cmbListaPaginas").value = paginaActual;
    cargarDatos(false);
}

function irInicio() {
    paginaActual = 1;
    document.getElementById("cmbListaPaginas").value = paginaActual;
    cargarDatos(false);
}

function seleccionPagina() {
    var select = document.getElementById("cmbListaPaginas");
    paginaActual = select.value;
    cargarDatos(false);
}

function cambiarTotalRegistrosMostrar() {
    filasxPagina = document.getElementById("cmbRegxPagina").value;
    paginaActual = 1;
    // Se debe calcular nuevamente el listado de paginas disponibles ya que cambio el tamaño del bloque a mostrar
    cargarDatos(true);
}

function cargaListaPaginas() {
    totalPaginas = Math.ceil(totalRegistros / filasxPagina);

    // Se agregan el listado de paginas disponibles dentro del combo de pagineo
    var optionsPagineo = document.getElementById("cmbListaPaginas");
    optionsPagineo.innerHTML = "";

    for (var i = 1; i <= totalPaginas; i++) {
        var option = document.createElement("option");
        option.value = i;
        option.textContent = ` ${i} / ${totalPaginas}`;
        optionsPagineo.appendChild(option);
    }
}

function cargarDatos(calcularPagineo) {
    var table = document.getElementById("tblData");
    var tbody = table.getElementsByTagName("tbody")[0];
    var entity = document.getElementById("entidad").value;

    tbody.innerHTML = "";


    if (calcularPagineo) {
        paginaActual = 1;
    } else {
        paginaActual = parseInt(document.getElementById("cmbListaPaginas").value)
        paginaActual = isNaN(paginaActual) ? 1 : paginaActual;
    }
    //filasxPagina = parseInt(document.getElementById("cmbRegxPagina").value);

    // Realizar la solicitud de los datos paginados
    fetch(`/${entity}/getListItems?page=${paginaActual}&perPage=${filasxPagina}`)
        .then(response => response.json())
        .then(data => {
            totalRegistros = data.total;
            debugger;
            // Se hace el calculo del pagineo cuando es requerido
            if (calcularPagineo) {
                cargaListaPaginas();
            }

            // Mostrar los datos segun la pagina actual segun los indices calculados dentro de los datos
            for (var i = 0; i < data.rows.length; i++) {
                var item = data.rows[i];
                var row = htmlRow(item);
                tbody.innerHTML += row;
            }
        })
        .catch(error => console.error("Error: ", error));
}


function nuevo() {
    var entity = document.getElementById("entidad").value;
    window.location.href = `/${entity}/Create`;
}


function nuevo() {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Create/`;
}

function editar(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Edit?id=` + id;
}

function verDetalle(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Details?id=` + id;
}

function eliminar(id) {
    var entidad = document.getElementById("entidad").value;
    window.location.href = `/${entidad}/Delete?id=` + id;
}

cargarDatos(true);
