﻿function cargarDatos() { 
    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');
    var entidad = document.getElementById("entidad").value;

    fetch(`/${entidad}/getItemsById/` + id)
        .then(response => response.json())
        .then(data => { 
            fillData(data);
        })
        .catch(error => console.error('Error', error));
}

function eliminar() {
    const id = document.getElementById('id').textContent;
    const token = document.querySelector('input[name="__RequestVerificationToken"]').value;
    var entidad = document.getElementById("entidad").value;

    const postData = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'RequestVerificationToken': token
        },
        body: JSON.stringify(id)
    };

    fetch(`/${entidad}/DeleteById/`, postData)
        .then(response => response.json())
        .then(data => {
            if (data.id == 0) {
                window.location.href = `/${entidad}/Items/`;
            }
            else {
                window.alert(data.msg);
            }
        })
        .catch(error => console.error('Error', error));
}
 
function cancelar() { 
    window.history.back();
}
