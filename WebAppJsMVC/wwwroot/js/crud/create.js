﻿// Función para guardar los cambios
function guardar(postData) {
    const token = document.querySelector('input[name="__RequestVerificationToken"]').value;
    var entidad = document.getElementById("entidad").value;

    // Configurar opciones de la solicitud
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'RequestVerificationToken': token // Incluir el token anti-falsificación como encabezado personalizado
        },
        body: JSON.stringify(postData)
    };

    // Hacer la solicitud POST 
    fetch(`/${entidad}/Create/`, requestOptions)
        .then(response => response.json())
        .then(data => {
            // Manejar la respuesta del servidor
            if (data.id == 0) {
                window.location.href = `/${entidad}/Items/`;
            }
            else {
                window.alert(data.msg);
            }
        })
        .catch(error => console.error('Error', error));
}
 
function cancelar() { 
    window.history.back();
}
