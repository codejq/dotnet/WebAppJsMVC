﻿// Función para cargar los datos del país en el formulario
function cargarDatos() {
    // Obtener el ID del país de los parámetros de la URL
    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');
    var entidad = document.getElementById("entidad").value;

    fetch(`/${entidad}/getItemsById/` + id)
        .then(response => response.json())
        .then(data => {
            fillData(data);
        })
        .catch(error => console.error('Error', error));
}

// Función para guardar los cambios
function guardar(postData) {
    var entidad = document.getElementById("entidad").value;
    const token = document.querySelector('input[name="__RequestVerificationToken"]').value;

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'RequestVerificationToken': token
        },
        body: JSON.stringify(postData)
    };

    fetch(`/${entidad}/Edit/`, requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data.id == 0) {
                window.location.href = `/${entidad}/Items/`;
            }
            else {
                window.alert(data.msg);
            }
        })
        .catch(error => console.error('Error', error));
}
 
function cancelar() { 
    window.history.back();
} 